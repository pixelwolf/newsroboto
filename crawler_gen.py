import random, os, json, datetime, time

from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
from bson import json_util

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

import os
import sys
import glob
import json
import datetime

import scrapy
from scrapy.settings import Settings
from scrapy.crawler import CrawlerRunner, CrawlerProcess
from scrapy.utils.project import get_project_settings

import subprocess

from crawler_api.crawler_api.spiders.feedley_spider import Feedly_Craler
from crawler_api.crawler_api.crawler_run import CrawlerRun

app = Flask(__name__)

client = MongoClient('mongo', 27017)
db_feed = client.feedly_db
db_twitt = client.twitt_db

time.sleep(5) # hack for the mongoDb database to get running

@app.route('/teste-feed')
def todo_feed():
    _items = db_feed.feedly_db.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/testeok')
def testeok():
    return "TesteOK"

@app.route('/teste-twitt')
def todo_twitt():
    _items = db_twitt.twitt_db.find()
    items = [item for item in _items[0:100]]

    return render_template('todo.html', items=items)

@app.route('/update')
def get():
    crawler_obj = CrawlerRun()
    crawler_obj.run()
    return render_template('blog/index.html')


@app.route('/twitter', methods=('GET', 'POST'))
def twitter():
    _items = db_twitt.twitt_db.find()
    items = [item for item in _items]

    news_list = json_reader(items)
    news_list = sorted(news_list, key=lambda k: k['ordered_time'], reverse=True)
    return render_template('display/twitter.html', posts=news_list)

@app.route('/feedly', methods=('GET', 'POST'))
def feedly():

    _items = db_feed.feedly_db.find()
    items = [item for item in _items]

    cat_list = []
    news_list = json_reader(items, True)
    news_list = sorted(items, key=lambda k: k['published'], reverse=True)

    for item in news_list:
        if not "categories" in item:
            item["categories"] = [{'label': 'Uncategorized', 'id': 'user/a1ab8f3a-8040-4e5b-802a-55c583f4abb1/category/global.uncategorized_tab_label'}]

        if item["categories"][0]['label'] not in cat_list:
            cat_list.append(item["categories"][0]['label'])

    return render_template('display/feedly.html', posts=news_list, categories=sorted(cat_list))

def json_reader(items, feedly=False):
    list_items = []
    count = 1
    for line in items:
        item = line
        if feedly:
            """ Convert Seconds to Date"""
            item['published'] = str(datetime.datetime.utcfromtimestamp(item['published']/1000).strftime('%Y-%m-%d %H:%M:%S'))
        list_items.append(item)
        if count == 1000:
            break
        count+=1
    return list_items

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.config['DEBUG'] = True
    app.run(host='0.0.0.0', port=port)
