# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import time
from fabric.api import cd, env, run, local, puts
from fabric.contrib.files import upload_template

env.root = os.path.dirname(os.path.abspath(__file__))
env.repo = 'git@bitbucket.org:pixelwolf/newsroboto.git'
env.project = 'newsroboto'
env.project_root = '/srv/%(project)s' % env
env.user = 'ubuntu'
env.roledefs = {
    'prod': [''],
    'stage': ['54.39.34.139'],
}

# https://news-roboto.pixelwolf.co:5000/feedly

def docker_compose(command):
    if env.roles[0] in ["prod", "stage"]:
    #     prefix = ('docker-compose -f docker-compose.yml -f '
    #               'docker-compose.prod.yml {}')
    # else:
        prefix = ('docker-compose {}')
    with cd(env.project_root):
        run(prefix.format(command))


def configure_step_1():
    run('cat /etc/default/grub')
    run("""sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet cgroup_enable=memory swapaccount=1"/g'  /etc/default/grub""")
    run('cat /etc/default/grub')
    run('sudo update-grub')
    run('sudo reboot')


def configure_step_2():
    generate_key()
    puts('Copy the ssh key and add in bitbucket.org on project repo.')
    run('read -p "Press any key to continue... " -n1 -s')
    run('sudo apt-get install apt-transport-https ca-certificates curl software-properties-common')
    run('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
    run('sudo apt-key fingerprint 0EBFCD88')
    run('sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
    run('sudo apt update')
    run('sudo apt upgrade -y')
    run('sudo apt install docker-ce -y')
    run('sudo apt install -y python3-pip')
    run('sudo pip3 install docker-compose')
    run('sudo chmod +x /usr/local/bin/docker-compose')
    run('sudo ln -s /usr/local/bin/docker-compose /bin/docker-compose')
    run('pip3 install --upgrade pip --user')
    run('sudo apt install -y htop')
    run('sudo usermod -aG docker $USER')
    run('sudo reboot')


def configure_step_3():
    run('sudo chown {0}:{0} /srv'.format(env.user))
    run('git clone %(repo)s %(project_root)s' % env)
    with cd(env.project_root):
        run('git checkout %s' % env.roles[0])


def generate_key():
    run('cat /dev/zero | ssh-keygen -q -N ""')
    run('cat ~/.ssh/id_rsa.pub')


def update_app(custom_branch=None):
    with cd(env.project_root):
        run('git fetch && git reset --hard')

        if custom_branch:
            puts('Exec deploy using CUSTOM MODE to branch %s' % custom_branch)
            run('git checkout -B {0} origin/{0} && git pull'.format(custom_branch))
        else:
            run('git checkout -B {0} origin/{0} && git pull'.format(env.roles[0]))


def update_secrets():
    if "prod" == env.roles[0]:
        upload_template('tmp/.env-prod', '%(project_root)s/.env' % env)
    else:
        upload_template('tmp/.env-stage', '%(project_root)s/.env' % env)


def stop_containers():
    docker_compose('stop')


def build_containers():
    docker_compose('build')


def start_containers():
    docker_compose('up -d')


def restart_containers():
    stop_containers()
    start_containers()


def attach_logs():
    # run('echo - n "Press SPACE to continue or Ctrl+C to exit ... "')
    # while true; do
    #     # Set IFS to empty string so that read doesn't trim
    #     # See http://mywiki.wooledge.org/BashFAQ/001#Trimming
    #     IFS = read - n1 - r key
    #     [[ $key == ' ']] & & break
    # done
    # echo
    # echo "Continuing ..."
    docker_compose('logs --tail 50')


def run_debug():
    local("docker-compose up -d")
    local("docker-compose stop web")
    local("docker-compose run --service-ports web")


def deploy(custom_branch=None):
    start = time.time()
    update_app(custom_branch)
    stop_containers()
    build_containers()
    start_containers()
    attach_logs()
    final = time.time()

    puts(" ________")
    puts('< execution finished in %.2fs >' % (final - start))
    puts(" --------")
    puts("      \ ")
    puts("        \ ")
    puts("          \ ")
    puts("                   ##        . ")
    puts("             ## ## ##       == ")
    puts("          ## ## ## ##      === ")
    puts('      /""""""""""""""""___/ === ')
    puts(" ~~~ {~~ ~~~~ ~~~ ~~~~ ~~ ~ /  ===- ~~~ ")
    puts("      \______ o          __/ ")
    puts("       \    \        __/ ")
    puts("        \____\________/ ")
